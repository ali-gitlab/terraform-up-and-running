terraform {
  backend "s3" {
    key = "prod/data-stores/mysql/terraform.tfstate"
  }
}

provider "aws" {
  region = "us-east-1"
  version = "~> 2.47"
}

module "mysql_database" {
  source = "../../../../modules/data-stores/mysql"

  db_password = var.db_password
  identifier_prefix = "prod-terraform-up-and-running"
  instance_class = "db.t2.micro"
}