resource "aws_db_instance" "example" {
  identifier_prefix = var.identifier_prefix
  engine = "mysql"
  allocated_storage = 10
  instance_class = var.instance_class
  name = "example_database"
  username = "admin"
  password = var.db_password
  skip_final_snapshot = true
}