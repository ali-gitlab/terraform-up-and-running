variable "db_password" {
  description = "The password for the database"
  type = string
}

variable "identifier_prefix" {
  description = "The prefix of the DB name"
  type = string
}

variable "instance_class" {
  description = "The instance class of the DB (e.g. db.t2.micro)"
  type = string
}